package com.example.demo;

import lombok.Data;

import java.util.Date;

@Data
class MyMessage {
    private Integer id;
    private String msg;
    private Date date;
}
